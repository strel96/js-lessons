const _createCard = function (computers){
    let div = document.createElement('div');
    div.classList.add('row');

    computers.forEach(comp => {
        div.insertAdjacentHTML('afterbegin', `
        <div class="col">
            <div class="card">
                <img src=${comp.src} class="card-img-top" style="height: 300px;">
                <div class="card-body">
                    <h5 class="card-title">${comp.title}</h5>
                    <p>Price: ${comp.price}</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>
    `)
        document.querySelector('.container').appendChild(div);
    });

    return div;
}

$.card = function (computers){
    let $cards = _createCard(computers);

}