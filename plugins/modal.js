Element.prototype.appendAfter = function (element){
    element.parentNode.insertBefore(this, element.nextSibling);
}

function _createFooterButtons(buttons = []){
    if(buttons.length === 0){
        return document.createElement('div');
    }

    let div = document.createElement('div');
    div.classList.add('modal-footer');

    let count = 1;
    buttons.forEach(button => {
        let $button = document.createElement('button');
        $button.classList.add('btn' + count++);
        $button.textContent = button.text;
        $button.onclick = button.handler;
        div.appendChild($button);
    });

    return div;
}

function _createModal (modalOptions) {
    let modal = document.createElement('div');
    modal.classList.add('vmodal');
    modal.insertAdjacentHTML('afterbegin', `
    <div class="modal-overlay" data-close="true">
        <div class="modal-window">
            <div class="modal-header">
                <span class="modal-title">${modalOptions.title}</span>
                <span class="modal-close" data-close="true">&times;</span>
            </div>
            <div class="modal-body"  data-body>
                <p>${modalOptions.content}</p>
                <p>${modalOptions.content}</p>
            </div>
        </div>
    </div>
   `)
    let footer = _createFooterButtons(modalOptions.buttons);
    footer.appendAfter(modal.querySelector('[data-body]'));
    document.body.appendChild(modal);

    return modal;
}

$.modal = function(modalOptions) {
    const $modal = _createModal(modalOptions);

    let modal = {
        open() {
            $modal.classList.add('open');
        },
        close() {
            $modal.classList.remove('open');
        }
    }

    const listener = event => {
        if (event.target.dataset.close) modal.close();
    }

    document.addEventListener('click', listener);

    return Object.assign(modal, {

    });
}
