const $ = {}

const computers = [
    {title: 'Macbook Pro 15', price: 3000, src: 'img/macbook.jpeg'},
    {title: 'Macbook Air 13', price: 1000, src: 'img/macbook.jpeg'},
    {title: 'Imac', price: 5000, src: 'img/macbook.jpeg'},
];

const modalOptions = [
    {
        title: 'Some Title',
        closeable: true,
        content: 'Some Content',
        width: '480px',
        buttons: [
            {
                text: 'Ok',
                handler() {
                    console.log('Button Ok clicked');
                    modal.close();
                }
            },
            {
                text: 'Cancel',
                handler() {
                    console.log('Button Cancel clicked');
                    modal.close();
                }
            }
        ]
    }
];